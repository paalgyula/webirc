package hu.paalgyula.irc.webchat;

import hu.paalgyula.irc.webchat.messages.ChannelMessage;
import org.jibble.pircbot.IrcException;
import org.jibble.pircbot.PircBot;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created with IntelliJ IDEA.
 * User: jethro.gibbs
 * Date: 2012.12.24.
 * Time: 10:29
 * To change this template use File | Settings | File Templates.
 */
@Controller
@Scope("session")
public class AjaxController extends PircBot {

    private Logger logger = Logger.getLogger(AjaxController.class.getName());
    private IRCUser user = new IRCUser();

    @ResponseBody
    @RequestMapping(value = "/q", method = RequestMethod.GET)
    public Map<String, Object> doQuery() {
        Map<String, Object> rmap = new HashMap<String, Object>();

        rmap.put("success", true);
        rmap.put( "messages", user.getMessages() );

        return rmap;
    }

    @ResponseBody
    @RequestMapping("/connect")
    public Map<String, Object> doConnect(
            @RequestParam("server") String server,
            @RequestParam(value = "port", required = false, defaultValue = "6667") Integer port,
            @RequestParam("nick") String nick
    ) {

        Map<String, Object> rmap = new HashMap<String, Object>();
        rmap.put("success", true);

        try {
            setName(nick);
            connect(server);
        } catch (IOException e) {
            rmap.put("success", false);
            rmap.put("error", e.getMessage());
        } catch (IrcException e) {
            rmap.put("success", false);
            rmap.put("error", e.getMessage());
        }

        return rmap;
    }

    @ResponseBody
    @RequestMapping("/join")
    public Map<String, Object> channelJoin(@RequestParam("channel") String channel, @RequestParam(value = "key", required = false, defaultValue = "") String key) {
        Map<String, Object> rmap = new HashMap<String, Object>();
        rmap.put("success", true);

        if (!key.isEmpty()) {
            joinChannel("#" + channel, key);
            logger.log(Level.INFO, "Joining channel: " + channel + " key: " + key);
        } else {
            joinChannel("#" + channel);
            logger.log(Level.INFO, "Joining channel: " + channel);
        }

        return rmap;
    }

    @Override
    protected void onMessage(String channel, String sender, String login, String hostname, String message) {
        super.onMessage(channel, sender, login, hostname, message);    //To change body of overridden methods use File | Settings | File Templates.
        user.getMessages().add( new ChannelMessage( channel, sender, message ) );
    }

    @Override
    protected void finalize() throws Throwable {
        disconnect();
    }

}
