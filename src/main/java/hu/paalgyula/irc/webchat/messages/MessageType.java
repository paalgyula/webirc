package hu.paalgyula.irc.webchat.messages;

/**
 * Created with IntelliJ IDEA.
 * User: jethro.gibbs
 * Date: 2012.12.24.
 * Time: 15:25
 * To change this template use File | Settings | File Templates.
 */
public enum MessageType {
    SERVER,
    PRIVATE,
    CHANNEL,
    NOTIFY,
    EMOTE
}
