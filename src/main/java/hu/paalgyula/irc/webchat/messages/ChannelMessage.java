package hu.paalgyula.irc.webchat.messages;

import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: jethro.gibbs
 * Date: 2012.12.24.
 * Time: 15:26
 * To change this template use File | Settings | File Templates.
 */
public class ChannelMessage implements Message {
    private Date date = new Date();
    private String message;
    private String channel;
    private MessageType messageType = MessageType.CHANNEL;
    private String sender;

    public ChannelMessage(String channel, String sender, String message) {
        this.channel = channel;
        this.message = message;
        this.sender = sender;
    }

    @Override
    public MessageType getMessageType() {
        return this.messageType;
    }

    @Override
    public String getMessage() {
        return this.message;
    }

    public String getChannel() {
        return channel;
    }

    public String getSender() {
        return sender;
    }

    @Override
    public Date getDate() {
        return this.date;
    }
}
