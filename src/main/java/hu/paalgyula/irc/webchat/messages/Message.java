package hu.paalgyula.irc.webchat.messages;

import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: jethro.gibbs
 * Date: 2012.12.24.
 * Time: 15:25
 * To change this template use File | Settings | File Templates.
 */
public interface Message {
    public Date getDate();
    public MessageType getMessageType();
    public String getMessage();
}
