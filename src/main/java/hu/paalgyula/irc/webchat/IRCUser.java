package hu.paalgyula.irc.webchat;

import hu.paalgyula.irc.webchat.messages.Message;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: jethro.gibbs
 * Date: 2012.12.24.
 * Time: 13:02
 * To change this template use File | Settings | File Templates.
 */
public class IRCUser {
    private String nickname;
    private String username;
    private String realname;

    private List<Message> messages = new ArrayList<Message>();

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getRealname() {
        return realname;
    }

    public void setRealname(String realname) {
        this.realname = realname;
    }

    public List<Message> getMessages() {
        return messages;
    }

    public void setMessages(List<Message> messages) {
        this.messages = messages;
    }
}
